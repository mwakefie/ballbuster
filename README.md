# README #

This repository contains the source code for an iPhone implementation of the popular brick breaking game, called BallBuster.  Simply drag your paddle and keep that ball bustin'.

![800x600.png](https://bitbucket.org/repo/nX5Rqz/images/1267466051-800x600.png)

So simple.  So hypnotic.  So addictive.  So FREE!

Grab your favorite finger and drag your paddle and watch that ball bust some squares.  Simply move your paddle under the bouncing ball and watch the ball bust objects above.  Great for kids or little ones that need a relatively easy challenge.  More surprises, features, and functionality to come.  

Go ahead and fork the source code and add your own features!

## Coming Soon ##

* Multiple balls
* Motion controls
* Pause gameplay
* Options
* Bonus items

Contact matt@wakefieldweb.net for support questions.