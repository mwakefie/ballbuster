//
//  LevelSeven.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the seventh level.
///
public struct LayoutSeven {
  
  // OO..OO.O.O
  // .O..O..O.O
  // ..OO...O.O
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, true, false, false, true, true, false, true, false, true];
    let rowTwo = [false, true, false, false, true, false, false, true, false, true];
    let rowThree = [false, false, true, true, false, false, false, true, false, true];
    return rowOne + rowTwo + rowThree;
  }
}