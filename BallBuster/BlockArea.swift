import SpriteKit;

/// 
/// An abstract representation of the game area containing breakable blocks.
///
public class BlockArea {
  
  private let id:UInt32 = 0x1 << 2;
  private let name:NSString = "block";
  private let blockWidth:CGFloat = 50;
  private let blockColor:UIColor = UIColor.orangeColor();
  private let padding:CGFloat = 10;
  private let columnsPerRow = 10;
  
  /// Adds blocks to a given game scene object.
  public func drawBlocks(scene:LevelScene) {
    
    // Calculate starting x position for block placement.
    let totalBlocksWidth = blockWidth * CGFloat(columnsPerRow);
    let totalPadding = padding * CGFloat(columnsPerRow - 1);
    let xOffset = (CGRectGetWidth(scene.frame) - totalBlocksWidth - totalPadding) / 2;
    
    // Initialize row counter.
    var row:Int = 0;
    
    // Loop through blocks.
    for blockNumber in 0..<scene.getLayout().count {
      
      // Define block node.
      let size = CGSizeMake(blockWidth, blockWidth);
      let node = SKSpriteNode(color: UIColor.clearColor(), size: size);
      
      // Calculate column position based on number of blocks, columns per row, and current row.
      var column = blockNumber - (columnsPerRow * row);
      
      // Calculate coordinate position.
      var y = CGRectGetHeight(scene.frame) * (1 - (CGFloat(row + 1) * 0.2));
      var x = xOffset + CGFloat(CGFloat(column) + 0.5) * blockWidth + CGFloat(column - 1) * padding;
      
      // Define area for rounded rectangle shape.
      let area = CGRectMake(-node.size.width / 2, -node.size.height / 2, node.size.width, node.size.height);
      let path:CGPathRef = CGPathCreateWithRoundedRect(area, 14, 14, nil);
      let roundedRectangle = SKShapeNode(rectOfSize: size);
      roundedRectangle.fillColor = blockColor;
      roundedRectangle.lineWidth = 0;
      roundedRectangle.path = path;
      node.addChild(roundedRectangle);
      
      // Define block properties.
      node.position = CGPointMake(x, y);
      node.physicsBody = SKPhysicsBody(polygonFromPath: path);
      node.physicsBody!.allowsRotation = false;
      node.physicsBody!.friction = 0.0;
      node.physicsBody!.affectedByGravity = false;
      node.name = name as String;
      node.physicsBody!.categoryBitMask = getId();
      node.physicsBody!.dynamic = false;
      
      // Add to scene, if specified by layout.
      if (scene.getLayout()[blockNumber] as! Bool) {
        scene.addChild(node);
      }
      
      // Increment row counter.
      if (column == columnsPerRow - 1) {
        row++;
      }
    }
  }
  
  /// Getter for the block area's unique identifier.
  public func getId() -> UInt32 {
    return id;
  }
  
  /// Getter for block name.
  public func getName() -> NSString {
    return name;
  }
}