//
//  LevelFive.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the fifth level.
///
public struct LayoutFive {
  
  // OO......OO
  // .OO....OO.
  // ..OO..OO..
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, true, false, false, false, false, false, false, true, true];
    let rowTwo = [false, true, true, false, false, false, false, true, true, false];
    let rowThree = [false, false, true, true, false, false, true, true, false, false];
    return rowOne + rowTwo + rowThree;
  }
}