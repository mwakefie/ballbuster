import SpriteKit;

///
/// An abstract representation of the game over zone at the bottom of the screen.
///
public class GameOverZone {
  
  private let id:UInt32 = 0x1 << 1;
  private let node:SKSpriteNode;
  
  /// Initialize the game over zone.
  public init() {
    
    // Define sprite node.
    let size:CGSize = CGSizeMake(1136, 50);
    node = SKSpriteNode(color: UIColor.clearColor(), size: size);
    node.name = "bottom";
    
    // Define physics body.
    let bottomBody:SKPhysicsBody = SKPhysicsBody(rectangleOfSize: size);
    bottomBody.usesPreciseCollisionDetection = true;
    bottomBody.dynamic = false;
    bottomBody.affectedByGravity = false;
    bottomBody.allowsRotation = false;
    bottomBody.pinned = false;
    bottomBody.linearDamping = 0;
    bottomBody.friction = 0;
    bottomBody.restitution = 1;
    bottomBody.angularDamping = 0;
    node.physicsBody = bottomBody;
    
    // Define position.
    node.position.x = 560;
    node.position.y = 0;
  }
  
  /// Getter for the game over zone unique identifier.
  public func getId() -> UInt32 {
    return id;
  }
  
  /// Getter for the game zone sprite node.
  public func getNode() -> SKSpriteNode {
    return node;
  }
}