//
//  LevelTwentySeven.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 27th level.
///
public struct LayoutTwentySeven {
  
  // OO......OO
  // OO......OO
  // OOOOOOOOOO
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, true, false, false, false, false, false, false, true, true];
    let rowTwo = [true, true, false, false, false, false, false, false, true, true];
    let rowThree = [true, true, true, true, true, true, true, true, true, true];
    return rowOne + rowTwo + rowThree;
  }
}