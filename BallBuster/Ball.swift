import SpriteKit;

///
/// An abstract representation of the bouncing game ball.
///
public class Ball {
  
  private let id:UInt32 = 0x1 << 0;
  private let radius:CGFloat = 15;
  private let color:UIColor = UIColor.redColor();
  private let node:SKSpriteNode;
  
  /// Initialize the ball.
  public init() {
    
    // Define sprite node.
    let size:CGSize = CGSizeMake(radius * 2, radius * 2);
    node = SKSpriteNode(color: UIColor.clearColor(), size:size);
    
    // Define physics body.
    let circleBody:SKPhysicsBody = SKPhysicsBody(circleOfRadius: radius);
    circleBody.usesPreciseCollisionDetection = true;
    circleBody.dynamic = true;
    circleBody.affectedByGravity = true;
    circleBody.allowsRotation = false;
    circleBody.pinned = false;
    circleBody.linearDamping = 0;
    circleBody.friction = 0.01;
    circleBody.restitution = 1;
    circleBody.angularDamping = 0;
    node.physicsBody = circleBody;
    
    // Define position.
    node.position.x = 450;
    node.position.y = 125;
    
    // Define area for circle shape inside ball.
    let area = CGRectMake(-node.size.width / 2, -node.size.height / 2, node.size.width, node.size.height);
    let path:CGPathRef = CGPathCreateWithEllipseInRect(area, nil);
    let circleShape = SKShapeNode(circleOfRadius: radius);
    circleShape.fillColor = color;
    circleShape.lineWidth = 0;
    circleShape.path = path;
    node.addChild(circleShape);
  }
  
  /// Getter for the ball's unique identifier.
  public func getId() -> UInt32 {
    return id;
  }
  
  /// Getter for the ball's sprite node.
  public func getNode() -> SKSpriteNode {
    return node;
  }
}