import Foundation;

///
/// A structure containing the layout for a given level.
///
public struct LayoutFactory {
  
  /// Returns the layout for a given level.
  public static func getLayout(level:Int) -> NSArray {
    var layout:NSArray = NSArray();
    switch (level) {
      case 1: layout = LayoutOne.getLayout();
      case 2: layout = LayoutTwo.getLayout();
      case 3: layout = LayoutThree.getLayout();
      case 4: layout = LayoutFour.getLayout();
      case 5: layout = LayoutFive.getLayout();
      case 6: layout = LayoutSix.getLayout();
      case 7: layout = LayoutSeven.getLayout();
      case 8: layout = LayoutEight.getLayout();
      case 9: layout = LayoutNine.getLayout();
      case 10: layout = LayoutTen.getLayout();
      case 11: layout = LayoutEleven.getLayout();
      case 12: layout = LayoutTwelve.getLayout();
      case 13: layout = LayoutThirteen.getLayout();
      case 14: layout = LayoutFourteen.getLayout();
      case 15: layout = LayoutFifteen.getLayout();
      case 16: layout = LayoutSixteen.getLayout();
      case 17: layout = LayoutSeventeen.getLayout();
      case 18: layout = LayoutEighteen.getLayout();
      case 19: layout = LayoutNineteen.getLayout();
      case 20: layout = LayoutTwenty.getLayout();
      case 21: layout = LayoutTwentyOne.getLayout();
      case 22: layout = LayoutTwentyTwo.getLayout();
      case 23: layout = LayoutTwentyThree.getLayout();
      case 24: layout = LayoutTwentyFour.getLayout();
      case 25: layout = LayoutTwentyFive.getLayout();
      case 26: layout = LayoutTwentySix.getLayout();
      case 27: layout = LayoutTwentySeven.getLayout();
      case 28: layout = LayoutTwentyEight.getLayout();
      case 29: layout = LayoutTwentyNine.getLayout();
      case 30: layout = LayoutThirty.getLayout();
      default: layout = LayoutOne.getLayout();
    }
    return layout;
  }
  
  /// Returns the total number of layouts.
  public static func getTotal() -> Int {
    return 30;
  }
}
