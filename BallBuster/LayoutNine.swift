//
//  LevelNine.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the ninth level.
///
public struct LayoutNine {
  
  // ..O.O..O..
  // ..O..OO...
  // ..O.O..O..
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [false, false, true, false, true, false, false, true, false, false];
    let rowTwo = [false, false, true, false, false, true, true, false, false, false];
    let rowThree = [false, false, true, false, true, false, false, true, false, false];
    return rowOne + rowTwo + rowThree;
  }
}