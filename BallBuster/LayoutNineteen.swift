//
//  LevelNineteen.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 19th level.
///
public struct LayoutNineteen {
  
  // .O.O.O.O.O
  // O.O.O.O.O.
  // .O.O.O.O.O
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [false, true, false, true, false, true, false, true, false, true];
    let rowTwo = [true, false, true, false, true, false, true, false, true, false];
    let rowThree = [false, true, false, true, false, true, false, true, false, true];
    return rowOne + rowTwo + rowThree;
  }
}