//
//  LevelTwentyThree.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 23rd level.
///
public struct LayoutTwentyThree {
  
  // OOOOOOOOOO
  // ....OO....
  // ....OO....
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, true, true, true, true, true, true, true, true, true];
    let rowTwo = [false, false, false, false, true, true, false, false, false, false];
    let rowThree = [false, false, false, false, true, true, false, false, false, false];
    return rowOne + rowTwo + rowThree;
  }
}