//
//  LevelTwelve.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 12th level.
///
public struct LayoutTwelve {
  
  // .O..O.O.O.
  // ..OO..O.O.
  // .O..O.O.O.
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [false, true, false, false, true, false, true, false, true, false];
    let rowTwo = [false, false, true, true, false, false, true, false, true, false];
    let rowThree = [false, true, false, false, true, false, true, false, true, false];
    return rowOne + rowTwo + rowThree;
  }
}