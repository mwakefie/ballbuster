//
//  LevelEight.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the eighth level.
///
public struct LayoutEight {
  
  // OOOO..OOOO
  // O..OOOO..O
  // OOOO..OOOO
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, true, true, true, false, false, true, true, true, true];
    let rowTwo = [true, false, false, true, true, true, true, false, false, true];
    let rowThree = [true, true, true, true, false, false, true, true, true, true];
    return rowOne + rowTwo + rowThree;
  }
}