//
//  LevelThirty.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 30th level.
///
public struct LayoutThirty {
  
  // OOOOOOOOOO
  // OOOOOOOOOO
  // OOOOOOOOOO
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, true, true, true, true, true, true, true, true, true];
    let rowTwo = [true, true, true, true, true, true, true, true, true, true];
    let rowThree = [true, true, true, true, true, true, true, true, true, true];
    return rowOne + rowTwo + rowThree;
  }
}