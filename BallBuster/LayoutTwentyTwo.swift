//
//  LevelTwentyTwo.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 22nd level.
///
public struct LayoutTwentyTwo {
  
  // OOOOOOOOOO
  // .OOO..OO..
  // .......OO.
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, true, true, true, true, true, true, true, true, true];
    let rowTwo = [false, true, true, true, false, false, true, true, false, false];
    let rowThree = [false, false, false, false, false, false, false, true, true, false];
    return rowOne + rowTwo + rowThree;
  }
}