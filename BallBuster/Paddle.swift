import SpriteKit;

///
/// An abstract representation of the game paddle.
///
public class Paddle {
  
  private let id:UInt32 = 0x1 << 3;
  private let name:NSString = "paddle";
  private let color:UIColor = UIColor.blueColor();
  private let node:SKSpriteNode;
  
  /// Initialize the paddle.
  public init() {
    
    // Define sprite node.
    let size:CGSize = CGSizeMake(200, 50);
    node = SKSpriteNode(color: UIColor.clearColor(), size: size);
    node.name = name as String;
    
    // Define area for rounded rectangle shape.
    let area = CGRectMake(-node.size.width / 2, -node.size.height / 2, node.size.width, node.size.height);
    let path:CGPathRef = CGPathCreateWithRoundedRect(area, 20, 20, nil);
    let roundedRectangle = SKShapeNode(rectOfSize: size);
    roundedRectangle.fillColor = color;
    roundedRectangle.lineWidth = 0;
    roundedRectangle.path = path;
    node.addChild(roundedRectangle);
    
    // Define physics body.
    let paddleBody:SKPhysicsBody = SKPhysicsBody(polygonFromPath: path);
    paddleBody.usesPreciseCollisionDetection = true;
    paddleBody.dynamic = false;
    paddleBody.affectedByGravity = false;
    paddleBody.allowsRotation = false;
    paddleBody.pinned = false;
    paddleBody.linearDamping = 0;
    paddleBody.friction = 0.01;
    paddleBody.restitution = 1;
    paddleBody.angularDamping = 0;
    node.physicsBody = paddleBody;
    
    // Define position.
    node.position.x = 510;
    node.position.y = 100;
  }
  
  /// Getter for the paddle's unique identifier.
  public func getId() -> UInt32 {
    return id;
  }
  
  /// Getter for paddle name.
  public func getName() -> NSString {
    return name;
  }
  
  /// Getter for the ball's sprite node.
  public func getNode() -> SKSpriteNode {
    return node;
  }
}
