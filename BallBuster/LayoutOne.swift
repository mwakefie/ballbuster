//
//  LevelOne.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the first level.
///
public struct LayoutOne {
  
  // O...OO...O
  // .O.O..O.O.
  // ..O....O..
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, false, false, false, true, true, false, false, false, true];
    let rowTwo = [false, true, false, true, false, false, true, false, true, false];
    let rowThree = [false, false, true, false, false, false, false, true, false, false];
    return rowOne + rowTwo + rowThree;
  }
}