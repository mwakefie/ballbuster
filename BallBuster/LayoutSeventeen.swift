//
//  LevelSeventeen.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 17th level.
///
public struct LayoutSeventeen {
  
  // OO..OO..OO
  // ..OO..OO..
  // OO..OO..OO
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, true, false, false, true, true, false, false, true, true];
    let rowTwo = [false, false, true, true, false, false, true, true, false, false];
    let rowThree = [true, true, false, false, true, true, false, false, true, true];
    return rowOne + rowTwo + rowThree;
  }
}