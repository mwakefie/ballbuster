//
//  LevelFourteen.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 14th level.
///
public struct LayoutFourteen {
  
  // .O.O...O..
  // .O.OOOOOO.
  // .O.....O..
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [false, true, false, true, false, false, false, true, false, false];
    let rowTwo = [false, true, false, true, true, true, true, true, true, false];
    let rowThree = [false, true, false, false, false, false, false, false, false, false];
    return rowOne + rowTwo + rowThree;
  }
}