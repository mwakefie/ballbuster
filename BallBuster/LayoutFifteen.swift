//
//  LevelFifteen.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 15th level.
///
public struct LayoutFifteen {
  
  // O.O.OO.O.O
  // O.O.OO.O.O
  // O.O.OO.O.O
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, false, true, false, true, true, false, true, false, true];
    let rowTwo = [true, false, true, false, true, true, false, true, false, true];
    let rowThree = [true, false, true, false, true, true, false, true, false, true];
    return rowOne + rowTwo + rowThree;
  }
}