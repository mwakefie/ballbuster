import UIKit;

///
/// Generates game alert messages.
///
public struct GameAlert {
  
  /// Show a game alert.
  public static func show(titleText:String,
                          buttonText:String,
                          action:Selector,
                          controller:UIViewController) -> UIView {
    
    // Create custom alert view.
    let view = UIView();
    
    // Create button.
    let button = UIButton();
    button.setTitle(buttonText, forState: UIControlState.Normal);
    button.addTarget(controller, action:action, forControlEvents: UIControlEvents.TouchUpInside);
    button.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 25);
    button.titleLabel?.sizeToFit();
    button.sizeToFit();
    button.layer.borderWidth = 1
    button.layer.cornerRadius = 10;
    button.layer.borderColor = UIColor.whiteColor().CGColor;
    button.frame.size.width += 20;
    
    // Position button.
    let titleWidth = (CGRectGetWidth(button.titleLabel!.frame) / 2);
    let bx = (CGRectGetWidth(controller.view.frame) / 2) - titleWidth - 10;
    let by = CGRectGetHeight(controller.view.frame) / 2;
    button.frame.offset(dx: bx, dy: by);
    
    // Create label.
    let label = UILabel();
    label.text = titleText;
    label.font = UIFont(name: "HelveticaNeue-Thin", size: 40);
    label.textColor = UIColor.darkGrayColor();
    label.sizeToFit();
                            
    // Position label.
    let labelWidth = (CGRectGetWidth(label.frame) / 2);
    let lx = (CGRectGetWidth(controller.view.frame) / 2) - labelWidth;
    let ly = CGRectGetHeight(controller.view.frame) / 3.2;
    label.frame.offset(dx: lx, dy: ly);
    
    // Assemble alert and return.
    view.addSubview(label);
    view.addSubview(button);
    view.backgroundColor = UIColor.lightGrayColor();
    return view;
  }
}