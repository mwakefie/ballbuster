import SpriteKit;
import AVFoundation;

///
/// A scene for each level of the game.
///
public class LevelScene:SKScene, SKPhysicsContactDelegate {
  
  private let level:Int;
  private let layout:NSArray;
  
  private let paddle:Paddle = Paddle();
  private let ball:Ball = Ball();
  private let bottom:GameOverZone = GameOverZone();
  private let blockArea:BlockArea = BlockArea();
  private let sidePanels:SidePanels = SidePanels();
  
  private let cup = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("cup", ofType: "mp3")!);
  private let tap = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("tap", ofType: "mp3")!);
  
  private var blockAudioPlayer = AVAudioPlayer();
  private var paddleAudioPlayer = AVAudioPlayer();
  
  private var isFingerOnPaddle:Bool = false;
  
  /// Implement initialization by size.
  public init(size:CGSize, level:Int, layout:NSArray) {
    self.level = level;
    self.layout = layout;
    super.init(size: size);
  }

  /// Implement initialization by coder object.
  public required init?(coder aDecoder:NSCoder) {
    fatalError("init(coder:) has not been implemented");
  }
  
  /// Setup game elements.
  public override func didMoveToView(view:SKView) {
    
    // Call super-class.
    super.didMoveToView(view);
    
    // Add game elements to scene.
    ball.getNode().physicsBody!.categoryBitMask = ball.getId();
    paddle.getNode().physicsBody!.categoryBitMask = paddle.getId();
    bottom.getNode().physicsBody!.categoryBitMask = bottom.getId();
    addChild(ball.getNode());
    addChild(paddle.getNode());
    addChild(bottom.getNode());
    addChild(sidePanels.getLeftPanelNode());
    addChild(sidePanels.getRightPanelNode());
    
    // Add border for bouncing ball.
    let border:CGRect = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height - 25);
    let borderBody = SKPhysicsBody(edgeLoopFromRect: border);
    borderBody.friction = 0;
    self.physicsBody = borderBody;
    self.backgroundColor = UIColor.whiteColor();
    
    // Remove gravity and apply force to ball.
    physicsWorld.gravity = CGVectorMake(0, 0);
    ball.getNode().physicsBody!.applyImpulse(CGVectorMake(5, -10));
    
    // Create blocks.
    blockArea.drawBlocks(self);
    
    // Detect collisions.
    physicsWorld.contactDelegate = self;
    ball.getNode().physicsBody!.contactTestBitMask = bottom.getId() | blockArea.getId() | paddle.getId();
    
    // Prepare sound for collisions.
    blockAudioPlayer = AVAudioPlayer(contentsOfURL: tap, error: nil);
    paddleAudioPlayer = AVAudioPlayer(contentsOfURL: cup, error: nil);
    blockAudioPlayer.prepareToPlay();
    paddleAudioPlayer.prepareToPlay();
  }
  
  /// Detect touch gesture within paddle area.
  public override func touchesBegan(touches:Set<NSObject>, withEvent event:UIEvent) {
    let touch = touches.first as! UITouch!;
    let touchLocation = touch.locationInNode(self);
    if ((physicsWorld.bodyAtPoint(touchLocation)) != nil) {
      let body = physicsWorld.bodyAtPoint(touchLocation);
      if (body?.node!.name == paddle.getName()) {
        isFingerOnPaddle = true;
      }
    }
  }
  
  /// Move the paddle based on touch movement detected.
  public override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
    if (isFingerOnPaddle) {
      
      // Get touch location.
      let touch = touches.first as! UITouch!;
      let touchLocation = touch.locationInNode(self);
      let previousLocation = touch.previousLocationInNode(self);
      
      // Calculate new position along x for paddle.
      var paddleX = paddle.getNode().position.x + (touchLocation.x - previousLocation.x);
      var paddleY = paddle.getNode().position.y + (touchLocation.y - previousLocation.y);
      
      // Limit x so that paddle won't leave screen to left or right.
      paddleX = max(paddleX, paddle.getNode().size.width / 2);
      paddleX = min(paddleX, size.width - paddle.getNode().size.width / 2);
      
      // Limit y so that the paddle won't move much up or down.
      paddleY = max(paddleY, 100);
      paddleY = min(paddleY, 200);
      
      // Update paddle position.
      paddle.getNode().position = CGPointMake(paddleX, paddleY);
    }
  }
  
  /// Reset variable when touch end detected.
  public override func touchesEnded(touches:Set<NSObject>, withEvent event:UIEvent) {
    isFingerOnPaddle = false;
  }
    
  /// Optimization logic to constrain maximum ball speed.
  public override func update(currentTime:NSTimeInterval) {
    let maxSpeed: CGFloat = 1000;
    let x = ball.getNode().physicsBody!.velocity.dx * ball.getNode().physicsBody!.velocity.dx;
    let y = ball.getNode().physicsBody!.velocity.dy * ball.getNode().physicsBody!.velocity.dy;
    let speed = sqrt(x + y);
    ball.getNode().physicsBody!.linearDamping = speed > maxSpeed ? 0.4 : 0.0;
  }
  
  /// Detect contact between two physics bodies.
  public func didBeginContact(contact:SKPhysicsContact) {
    
    // Define two physics bodies.
    var firstBody:SKPhysicsBody;
    var secondBody:SKPhysicsBody;
    
    // Assign the two physics bodies so that the one with the lower category is always stored in firstBody.
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
      firstBody = contact.bodyA;
      secondBody = contact.bodyB;
    } else {
      firstBody = contact.bodyB;
      secondBody = contact.bodyA;
    }
    
    // Check for paddle and ball collision.
    if (firstBody.categoryBitMask == ball.getId() && secondBody.categoryBitMask == paddle.getId()) {
      paddleAudioPlayer.play();
    }
    
    // React to the contact between ball and bottom.
    if (firstBody.categoryBitMask == ball.getId() && secondBody.categoryBitMask == bottom.getId()) {
      NSNotificationCenter.defaultCenter().postNotificationName("GameOver", object: self);
      self.paused = true;
    }
    
    // Check if the game has been won.
    if (firstBody.categoryBitMask == ball.getId() && secondBody.categoryBitMask == blockArea.getId()) {
      blockAudioPlayer.play();
      secondBody.node!.removeFromParent();
      if (isLevelWon()) {
        NSNotificationCenter.defaultCenter().postNotificationName("LevelComplete", object: self);
        self.paused = true;
      }
    }
  }
  
  /// Determine whether all blocks have been busted by the game ball.
  private func isLevelWon() -> Bool {
    var numberOfBricks = 0;
    self.enumerateChildNodesWithName(blockArea.getName() as String) {
      node, stop in
      numberOfBricks = numberOfBricks + 1;
    }
    return numberOfBricks == 0;
  }
  
  /// The current level.
  public func getLevel() -> Int {
    return level;
  }
  
  /// The level configuration.
  public func getLayout() -> NSArray {
    return layout;
  }
}