import SpriteKit;

///
/// An abstract representation of the side panels of the game area.
///
public class SidePanels {

  private let color:UIColor = UIColor.blackColor();
  
  private var leftNode:SKSpriteNode;
  private var rightNode:SKSpriteNode;
  
  /// Initialize the left node.
  public init() {
    leftNode = SKSpriteNode();
    rightNode = SKSpriteNode();
    initLeftNode();
    initRightNode();
  }
  
  private func initLeftNode() {
    let size:CGSize = CGSizeMake(50, 500);
    leftNode = SKSpriteNode(color: UIColor.clearColor(), size: size);
    let bezier:UIBezierPath = UIBezierPath();
    bezier.moveToPoint(CGPointMake(0, 0));
    bezier.addLineToPoint(CGPointMake(50, 500));
    bezier.addLineToPoint(CGPointMake(0, 500));
    bezier.closePath();
    let path = bezier.CGPath;
    let roundedRectangle = SKShapeNode(rectOfSize: size);
    roundedRectangle.fillColor = color;
    roundedRectangle.lineWidth = 0;
    roundedRectangle.path = path;
    leftNode.addChild(roundedRectangle);
    leftNode.physicsBody = getBody(path);
    leftNode.position.x = 0;
    leftNode.position.y = 200;
  }
  
  private func initRightNode() {
    let size:CGSize = CGSizeMake(50, 500);
    rightNode = SKSpriteNode(color: UIColor.clearColor(), size: size);
    let bezier:UIBezierPath = UIBezierPath();
    bezier.moveToPoint(CGPointMake(50, 0));
    bezier.addLineToPoint(CGPointMake(50, 500));
    bezier.addLineToPoint(CGPointMake(0, 500));
    bezier.closePath();
    let path = bezier.CGPath;
    let roundedRectangle = SKShapeNode(rectOfSize: size);
    roundedRectangle.fillColor = color;
    roundedRectangle.lineWidth = 0;
    roundedRectangle.path = path;
    rightNode.addChild(roundedRectangle);
    rightNode.physicsBody = getBody(path);
    rightNode.position.x = 986;
    rightNode.position.y = 200;
  }
  
  private func getBody(path:CGPathRef) -> SKPhysicsBody {
    let body:SKPhysicsBody = SKPhysicsBody(polygonFromPath: path);
    body.usesPreciseCollisionDetection = true;
    body.dynamic = false;
    body.affectedByGravity = false;
    body.allowsRotation = false;
    body.pinned = false;
    body.linearDamping = 0;
    body.friction = 0.01;
    body.restitution = 1;
    body.angularDamping = 0;
    return body;
  }

  /// Getter for the left panel sprite node.
  public func getLeftPanelNode() -> SKSpriteNode {
    return leftNode;
  }
  
  /// Getter for the left panel sprite node.
  public func getRightPanelNode() -> SKSpriteNode {
    return rightNode;
  }
}