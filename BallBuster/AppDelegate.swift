import UIKit;
import SpriteKit;

///
/// The main application delegate.
///
@UIApplicationMain
public class AppDelegate: UIResponder, UIApplicationDelegate {

  /// The main window.
  public var window:UIWindow?;

  /// Reports finished launch.
  public func application(application: UIApplication,
              didFinishLaunchingWithOptions launchOptions: [NSObject:AnyObject]?) -> Bool {
    
    // Application launched successfully.
    return true;
  }
  
  /// Pause game when no longer active.
  public func applicationWillResignActive(application: UIApplication) {
    
    // Pause game.
    let view:SKView = self.window?.rootViewController?.view as! SKView;
    view.paused = true;
  }
  
  /// Resume game when appropriate.
  public func applicationWillEnterForeground(application: UIApplication) {

    // Dismiss remaining alert and restart, if still presented.
    let controller = self.window?.rootViewController as! GameViewController;
    if (controller.presentedViewController != nil) {
      controller.resumeGame();
      controller.dismissViewControllerAnimated(false, completion: nil);
    }
  }
}