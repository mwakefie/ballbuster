//
//  LevelTwentyOne.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 21st level.
///
public struct LayoutTwentyOne {
  
  // ..OO..OO..
  // .OO....OO.
  // OO......OO
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [false, false, true, true, false, false, true, true, false, false];
    let rowTwo = [false, false, false, false, false, false, false, false, false, false];
    let rowThree = [true, true, false, false, false, false, false, false, true, true];
    return rowOne + rowTwo + rowThree;
  }
}