//
//  LayoutTwo.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the second level.
///
public struct LayoutTwo {
  
  // O........O
  // O........O
  // O........O
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, false, false, false, false, false, false, false, false, true];
    let rowTwo = [true, false, false, false, false, false, false, false, false, true];
    let rowThree = [true, false, false, false, false, false, false, false, false, true];
    return rowOne + rowTwo + rowThree;
  }
}