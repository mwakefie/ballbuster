//
//  LevelSix.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the sixth level.
///
public struct LayoutSix {
  
  // O........O
  // OOOOOOOOOO
  // ....OO....
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, false, false, false, false, false, false, false, false, true];
    let rowTwo = [true, true, true, true, true, true, true, true, true, true];
    let rowThree = [false, false, false, false, true, true, false, false, false, false];
    return rowOne + rowTwo + rowThree;
  }
}