//
//  GameViewController.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 12/31/14.
//  Copyright (c) 2014 MVW Technologies. All rights reserved.
//

import UIKit;
import SpriteKit;

/// 
/// Main view controller for game.
///
public class GameViewController:UIViewController {
  
  private var level:Int = 1;
  
  /// Hide status bar.
  public override func prefersStatusBarHidden() -> Bool {
    return true;
  }
  
  /// Setup the scene.
  public override func viewDidLoad() {
    super.viewDidLoad();
    self.view = GameAlert.show("BallBustin", buttonText: "Start", action: "onStart:", controller: self);
  }
  
  /// Start a specific level.
  private func startLevel(level:Int) {
    
    // Configure the game view and present level scene.
    let skView = SKView();
    self.view = skView;
    let size = CGSize(width: 1036, height: 640);
    let scene:LevelScene = LevelScene(size: size, level: level, layout: LayoutFactory.getLayout(level));
    scene.scaleMode = .AspectFill;
    skView.presentScene(scene);
      
    // Listen for end of game.
    let center = NSNotificationCenter.defaultCenter();
    center.addObserver(self, selector: "alertGameOver", name: "GameOver", object: nil);
    center.addObserver(self, selector: "alertLevelComplete", name: "LevelComplete", object: nil);
  }
  
  /// Construct and present game alert.
  private func alert(title:String, button:String, action:Selector) {
    let controller:UIViewController = UIViewController();
    controller.view = GameAlert.show(title, buttonText: button, action: action, controller: self);
    self.presentViewController(controller, animated: true, completion: nil);
  }
  
  /// Simple alert notifying the player they completed a level.
  internal func alertLevelComplete() {
    var title = "Level \(level) Complete";
    var button = "Next Level";
    if (level == LayoutFactory.getTotal()) {
      level = 0;
      title = "Game Complete";
      button = "Start Over";
    }
    alert(title, button: button, action: "onNextLevel:");
  }
  
  /// Simple alert notifying the player they lost the game.
  internal func alertGameOver() {
    let title = "Game Over";
    let button = "Try Again";
    alert(title, button: button, action: "onTryAgain:");
  }
  
  /// Button listener to start the game.
  public func onStart(sender:UIButton!) {
    restartLevel();
  }
  
  /// Button listener to restart the level after a try again alert.
  public func onTryAgain(sender:UIButton!) {
    self.dismissViewControllerAnimated(true, completion: nil);
    restartLevel();
  }
  
  /// Button listener to continue to next level after a level completion alert.
  public func onNextLevel(sender:UIButton!) {
    self.dismissViewControllerAnimated(true, completion: nil);
    level++;
    startLevel(level);
  }
  
  /// Restart the current level.
  public func restartLevel() {
    startLevel(level);
  }
  
  /// Resume the game.
  public func resumeGame() {
    var alertController:UIViewController = self.presentedViewController! as UIViewController;
    let label = alertController.view.subviews.first as! UILabel;
    if (label.text == "Game Over") {
      restartLevel();
    } else if (label.text == "Level \(level) Complete") {
      level++;
      startLevel(level);
    }
  }
}