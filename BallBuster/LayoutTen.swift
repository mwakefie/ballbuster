//
//  LevelTen.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the tenth level.
///
public struct LayoutTen {
  
  // OOOOOOOOOO
  // O........O
  // OOOOOOOOOO
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, true, true, true, true, true, true, true, true, true];
    let rowTwo = [true, false, false, false, false, false, false, false, false, true];
    let rowThree = [true, true, true, true, true, true, true, true, true, true];
    return rowOne + rowTwo + rowThree;
  }
}