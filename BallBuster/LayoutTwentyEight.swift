//
//  LevelTwentyEight.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 28th level.
///
public struct LayoutTwentyEight {
  
  // O..O..O..O
  // O.O.OO.O.O
  // O..O..O..O
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, false, false, true, false, false, true, false, false, true];
    let rowTwo = [true, false, true, false, true, true, false, true, false, true];
    let rowThree = [true, false, false, true, false, false, true, false, false, true];
    return rowOne + rowTwo + rowThree;
  }
}