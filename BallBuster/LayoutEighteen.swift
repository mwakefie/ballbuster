//
//  LevelEighteen.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the 18th level.
///
public struct LayoutEighteen {
  
  // O.O.O.O.O.
  // .O.O.O.O.O
  // O.O.O.O.O.
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, false, true, false, true, false, true, false, true, false];
    let rowTwo = [false, true, false, true, false, true, false, true, false, true];
    let rowThree = [true, false, true, false, true, false, true, false, true, false];
    return rowOne + rowTwo + rowThree;
  }
}