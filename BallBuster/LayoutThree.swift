//
//  LayoutThree.swift
//  BallBuster
//
//  Created by Matthew Wakefield on 2/2/15.
//  Copyright (c) 2015 MVW Technologies. All rights reserved.
//

import Foundation

///
/// An abstract representation of the third level.
///
public struct LayoutThree {
  
  // O...OO...O
  // O...OO...O
  // O...OO...O
  
  /// The layout for the level.
  public static func getLayout() -> NSArray {
    let rowOne = [true, false, false, false, true, true, false, false, false, true];
    let rowTwo = [true, false, false, false, true, true, false, false, false, true];
    let rowThree = [true, false, false, false, true, true, false, false, false, true];
    return rowOne + rowTwo + rowThree;
  }
}