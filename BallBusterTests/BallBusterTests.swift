//
//  BallBusterTests.swift
//  BallBusterTests
//
//  Created by Matthew Wakefield on 12/31/14.
//  Copyright (c) 2014 MVW Technologies. All rights reserved.
//

import XCTest;
import BallBuster;

public class BallBusterTests: XCTestCase {
    
  public override func setUp() {
    super.setUp();
  }
    
  public override func tearDown() {
    super.tearDown();
  }
    
  public func testUniqueLevels() {
    for (var level:Int = 1; level < 30; level++) {
      let one = LayoutFactory.getLayout(level);
      for (var otherLevel:Int = 1; otherLevel < 30; otherLevel++) {
        let two = LayoutFactory.getLayout(otherLevel);
        if (level != otherLevel) {
          let message:String = "Duplicate Levels: " + String(level) + "-" + String(otherLevel);
          XCTAssertNotEqual(one, two, message);
        }
      }
    }
  }
}
